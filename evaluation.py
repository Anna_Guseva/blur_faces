import os
import numpy as np
import cv2
from tqdm import trange

from src.extra_functions import plot_results, yolo_coords2xyxy, xyxy2yolo_coords

IMG_WIDTH, IMG_HEIGHT = 640, 480


def get_max_iou(pred_boxes, gt_box):
    """
    calculate the iou multiple pred_boxes and 1 gt_box (the same one)
    pred_boxes: multiple predict  boxes coordinate
    gt_box: ground truth bounding  box coordinate
    return: the max overlaps about pred_boxes and gt_box
    """
    # 1. calculate the inters coordinate
    if pred_boxes.shape[0] > 0:
        ixmin = np.maximum(pred_boxes[:, 0], gt_box[0])
        ixmax = np.minimum(pred_boxes[:, 2], gt_box[2])
        iymin = np.maximum(pred_boxes[:, 1], gt_box[1])
        iymax = np.minimum(pred_boxes[:, 3], gt_box[3])

        iw = np.maximum(ixmax - ixmin + 1., 0.)
        ih = np.maximum(iymax - iymin + 1., 0.)

    # 2.calculate the area of inters
        inters = iw * ih

    # 3.calculate the area of true
        gt_area = (gt_box[2] - gt_box[0] + 1.) * (gt_box[3] - gt_box[1] + 1.)

    # 4.calculate the area of union
        uni = ((pred_boxes[:, 2] - pred_boxes[:, 0] + 1.) * (pred_boxes[:, 3] - pred_boxes[:, 1] + 1.) +
               gt_area - inters)

    # 5.calculate the overlaps and find the max overlap ,the max overlaps index for pred_box
        iou = inters / uni
        metrics = {
            "iou": iou,
            "iou_max": np.max(iou),
            "nmax": np.argmax(iou),
            "gt_pred_overlap": np.max(inters / gt_area)
        }
        return metrics
    else:
        return {
            "iou": 0.,
            "iou_max": 0.,
            "nmax": None,
            "gt_pred_overlap": 0.
        }


if __name__ == "__main__":
    # obligatory
    images_path = "data/images_24_final"
    labels_path = "data/labels/with_faces"
    # pred_path = "models/yolov4/results_txt/test"
    pred_path = "output/with_faces/yolov3_yolov4_custom"
    # not obligatory
    images_save_path = "output/subclip_people/img/all"
    images_fn_save_path = "output/subclip_people/img/FN"
    images_tp_save_path = "output/subclip_people/img/TP"

    to_plot = True

    TP, FP, FN = 0, 0, 0
    labels_filenames = os.listdir(labels_path)
    for ind in trange(len(labels_filenames)):
        label_filename = labels_filenames[ind]
        image_name = os.path.splitext(label_filename)[0]

        # read true labels
        with open(os.path.join(labels_path, label_filename), "r") as file:
            true = file.read()
        true = true.split('\n')[:-1]
        if len(true) > 0:
            true = np.array(list(map(lambda x: x.split(" "), true)))[:, 1:].astype(float)
            # convert to xyxy
            true_xyxy = np.array(yolo_coords2xyxy(true, IMG_HEIGHT, IMG_WIDTH))

            # read images
            image_filename = f"{image_name}.jpg"
            image = cv2.imread(os.path.join(images_path, image_filename))
            pred_coords = np.array([])

            # read pred labels
            pred_filepath = os.path.join(pred_path, label_filename)
            if os.path.exists(pred_filepath):
                with open(pred_filepath, "r") as file:
                    pred = file.read()
                if len(pred) > 0:
                    pred = pred.split('\n')[:-1]
                    pred = np.array(list(map(lambda x: x.split(" "), pred))).astype(float)
                    pred_coords, pred_confs = pred[:, 1:], pred[:, 0]
                    pred_xyxy = np.array(yolo_coords2xyxy(pred_coords, IMG_HEIGHT, IMG_WIDTH))

                    # evaluate
                    FN_current = False
                    for true_bbox in true_xyxy:
                        metrics = get_max_iou(pred_xyxy, true_bbox)
                        if metrics["gt_pred_overlap"] < 0.5:
                            FN += 1
                            FN_current = True
                        else:
                            TP += 1

                    # plot
                    if FN_current:
                        plot_results(image, image_name,
                                     true_xyxy.astype(int),
                                     pred_xyxy.astype(int),
                                     save_path=None,  # os.path.join(images_fn_save_path, image_filename),
                                     to_show=False)
                    else:
                        plot_results(image, image_name,
                                     [],  # true_xyxy.astype(int),
                                     pred_xyxy.astype(int),
                                     save_path=None,  # os.path.join(images_tp_save_path, image_filename),
                                     to_show=False)

                    pass
                else:
                    FN += true_xyxy.shape[0]
            else:
                pass
                # FN += true_xyxy.shape[0]

        # plot_results(image, image_name,
        #              [],  # true_xyxy.astype(int),
        #              pred_coords.astype(int),
        #              save_path=os.path.join(images_save_path, image_filename),
        #              to_show=False)

    print("TP = ", TP)
    print("FN = ", FN)
    print("recall = ", TP / (TP + FN))
