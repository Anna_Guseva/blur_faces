from pathlib import Path
import cv2
import shutil
import subprocess
import pandas as pd
import numpy as np
from natsort import natsort_keygen, natsorted
from tqdm import trange
from moviepy.editor import VideoFileClip   # , ImageSequenceClip

from constants import *
from src.coordinates_functions import yolo_coords2xyxy, non_max_suppression, check_bboxes_area
from src.plot_functions import plot_results, blur_rois
from src.ImageSequenceClip import ImageSequenceClip
from models.trackers.track import track_objects
from src.plot_functions import blur_rois


def clean_folder(path):
    """
    :param path: (str) path to folder for cleaning
    """
    print("cleaning the", path)
    files = os.listdir(path)
    for f in files:
        shutil.rmtree(os.path.join(path, f))


def prepare_prediction(pred_path, imgs_height, imgs_width,
                       area_threshold=None, only_faces=True, nms=True):
    """
    Gets prediction made by YOLOv4;
    leaves only person class objects;
    drops objects less than area_threshold;
    makes non-maximum suppression
    keeps only person face

    :param pred_path: (str) path to the file made by YOLO
    :param imgs_height: (int) the height of the images
    :param imgs_width: (int) the width of the images
    :param area_threshold: (int) the minimum area of the detected object (in pixels)
    :param only_faces: (bool) keep only faces or the whole person
    :param nms: (bool) use non-maximum suppression
    :return: (dict) dictionary with predictions
    """
    prediction = {
        "filenames": [],
        "coords": [],
        "confs": []
    }

    preds = pd.read_json(pred_path)
    preds = preds.sort_values(by="filename", key=natsort_keygen()).reset_index(drop=True)

    for ind in trange(preds.shape[0], desc="blur images"):
        row = preds.iloc[ind, :]

        # get image filename
        img_filepath, objects = row["filename"], row['objects']

        # leave only people class
        objects = list(filter(lambda x: x["class_id"] == 0, objects))

        # get predicted coordinates and confidence
        relative_coords = list(map(lambda x: list(x["relative_coordinates"].values()), objects))
        confs = np.array(list(map(lambda x: x["confidence"], objects)))
        coords = yolo_coords2xyxy(relative_coords, imgs_height, imgs_width)
        coords, confs = check_bboxes_area(coords, confs, area_threshold)
        coords, confs = non_max_suppression(coords, confs)

        # gather all data to dict
        prediction["filenames"].extend([img_filepath] * coords.shape[0])
        prediction["coords"].extend(coords)
        prediction["confs"].extend(confs)

        # person to face bbox
        if only_faces and len(coords) > 0:
            # take upper 1/3 of the pred bboxes
            coords[:, 3] = coords[:, 1] + (coords[:, 3] - coords[:, 1]) / 3.
            # # make width thinner
            # drop_width_len = (coords[:, 2] - coords[:, 0]) / 5.
            # coords[:, 0] += drop_width_len
            # coords[:, 2] -= drop_width_len

    assert len(prediction["filenames"]) == len(prediction["coords"]) == len(prediction["confs"]), \
        "Different lengths in the prediction (dict) values"

    prediction["coords"] = np.array(prediction["coords"])

    return prediction


def prepare_tracking_df(prediction_dict):
    """
    Create the dataframe needed for tracking
    :param prediction_dict: (dict) with prepared predictions
    :return: (pd.DataFrame) dataframe for tracking
    """
    lefts, tops, rights, bottoms = prediction_dict["coords"][:, 0], prediction_dict["coords"][:, 1], \
                                   prediction_dict["coords"][:, 2], prediction_dict["coords"][:, 3]

    df = pd.DataFrame()
    df["Image_file"] = prediction_dict["filenames"]
    df["box_class"] = "Face"
    df["network"] = ""
    df["class_num"] = 0
    df["left"] = lefts
    df["top"] = tops
    df["right"] = rights
    df["bottom"] = bottoms
    df["Confidence"] = prediction_dict["confs"]
    df["Was_detected"] = True
    df["Track_id"] = None
    df["Track_conf"] = None
    df["network_folder"] = ""
    df["NMS_maked"] = None
    df["Display"] = None

    return df


def blur_images(images_filepaths, objects_df,
                plot_bboxes=False, to_blur=False, save_path=None):
    """
    Save images with the blurred/bboxed objects

    :param images_filepaths: (list) with fullpathes to images
    :param objects_df: (pd.DataFrame) dataframe with detected objects
    :param plot_bboxes: (bool) if to plot bounding boxes on images
    :param to_blur: (bool) if to blur detected objects on images
    :param save_path: (str) path to the folder for saving images
    """
    for ind in trange(len(images_filepaths), desc="blur images"):
        image_filepath = images_filepaths[ind]
        image_filename = Path(image_filepath).name

        # read image
        image = cv2.imread(image_filepath)
        # image = cv2.cvtColor(image_bgr, cv2.COLOR_BGR2RGB)
        # img_height, img_width, img_channels = image.shape

        rows = objects_df[objects_df["Image_file"] == image_filepath]
        coords = rows[["left", "top", "right", "bottom"]].values
        confs = rows["Confidence"].values

        if plot_bboxes:
            image = plot_results(image, "", [], coords, confs, to_show=False)

        if to_blur:
            image = blur_rois(image, coords)

        if save_path is not None:
            cv2.imwrite(os.path.join(save_path, image_filename), image)


if __name__ == "__main__":
    # data_path = "output/temp/images"
    data_path = "data/video_part2.mp4"
    save_path = os.path.join("output", Path(data_path).name)
    # save_path = os.path.join("output", "test.mp4")
    slice_num = None

    DETECTION_COEFF = 1.

    if not os.path.isdir(data_path):
        images_folder = os.path.join(CURRENT_PATH, TEMP_FOLDER, "images")
        # clean_folder(TEMP_FOLDER)
        if not os.path.exists(images_folder):
            os.mkdir(images_folder)

        video = VideoFileClip(data_path)
        audio = video.audio
        frames_num = video.reader.nframes

        extract_images = True  # temp variable
        if extract_images:
            video_iter = video.iter_frames()
            for ind in trange(frames_num, desc="extracting images"):
                try:
                    image_rgb = next(video_iter)
                    image = cv2.cvtColor(image_rgb, cv2.COLOR_RGB2BGR)
                    cv2.imwrite(os.path.join(images_folder, f"{ind}.jpg"), image)
                except Exception as error:
                    print(error)
    else:
        images_folder = data_path

    # create the file with the images
    images_filenames = natsorted(os.listdir(images_folder))
    images_file_path = os.path.join(TEMP_FOLDER, "images.txt")
    print("create file with images names")
    with open(images_file_path, "w") as file:
        for ind, filename in enumerate(images_filenames):
            if ind % DETECTION_COEFF == 0.:
                line = os.path.join(CURRENT_PATH, images_folder, filename).replace("\\", "/")
                file.writelines(line + "\n")

    # predict
    preds_filepath = os.path.join(TEMP_FOLDER, "results.json")
    to_predict = True  # temp variable
    if to_predict:
        print("predict")
        yolo_cfg = YOLO_COCO_CFG  # temp variable
        subprocess.run([yolo_cfg["darknet_path"],
                        "detector", "test",
                        yolo_cfg["obj_data"], yolo_cfg["configs"], yolo_cfg["weights"],
                        "-ext_output", "-dont_show", "-out",
                        preds_filepath, "<", images_file_path],
                       shell=True)

    # get images proportions
    img = cv2.imread(os.path.join(CURRENT_PATH, images_folder, images_filenames[0]))
    img_height, img_width, img_channels = img.shape
    print("image shape =", img_height, img_width, img_channels)

    # read and prepare prediction
    preds_dict = prepare_prediction(preds_filepath, img_height, img_width,
                                    area_threshold=10, only_faces=True, nms=True)

    # track objects
    objects_df = prepare_tracking_df(preds_dict)

    tracked_df = track_objects(objects_df, images_folder)
    # tracked_df.to_csv(os.path.join(TEMP_FOLDER, "tracked_df.csv"), index=False)
    # tracked_df = pd.read_csv("output/tracked_df.csv")
    tracked_df = tracked_df.sort_values(by="Image_file", key=natsort_keygen()).reset_index(drop=True)

    # blur images
    save_blurred_path = os.path.join(TEMP_FOLDER, "images_blurred")
    images_filepaths = list(map(lambda x: os.path.join(images_folder, x).replace("\\", "/"), images_filenames))
    blur_images(images_filepaths, tracked_df, plot_bboxes=False, to_blur=True, save_path=save_blurred_path)

    # write the blurred images
    if not os.path.isdir(save_path):
        print("\nstart saving the blurred video")
        # video_blurred = ImageSequenceClip(images_blurred, fps=video.fps)
        video_blurred = ImageSequenceClip(save_blurred_path, fps=video.fps)
        video_blurred = video_blurred.set_audio(audio)
        video_blurred.write_videofile(save_path)
        print("the blurred video was saved to", save_path)
    else:
        for ind in trange(len(images_filenames), desc="saving blurred images"):
            # image, image_filename = images_blurred[ind], images_filenames[ind]
            # cv2.imwrite(os.path.join(save_path, image_filename), image)
            pass

    pass
