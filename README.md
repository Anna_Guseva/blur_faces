# Face blurring*
*test project without models and data

Blur people faces on the low-resolution videos. 
Uses YOLOv4 and openCV+dlib trackers 

**main.py** - takes the path to video as an input saves the videos with the blurred faces as the output
