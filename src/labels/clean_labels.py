import os
from tqdm import trange

if __name__ == "__main__":
    labels_path = "../../data/labels/with_faces"
    filenames = os.listdir(labels_path)

    images_filenames = list(filter(lambda x: os.path.splitext(x)[1] == ".jpg", filenames))
    for ind in trange(len(images_filenames)):
        image_filename = images_filenames[ind]
        os.remove(os.path.join(labels_path, image_filename))

    pass
