import os
import cv2

from src.extra_functions import plot_results, yolo_coords2xyxy, read_yolo_file

if __name__ == "__main__":
    images_path = "../../data/images_24_final"
    labels_path = "../../data/labels/with_faces"
    labels_filenames = os.listdir(labels_path)

    for label_filename in labels_filenames:
        # read labels
        labels = read_yolo_file(os.path.join(labels_path, label_filename), drop_classes=True)
        labels_xyxy = yolo_coords2xyxy(labels, 480, 640)

        # read image
        name = os.path.splitext(label_filename)[0]
        image = cv2.imread(os.path.join(images_path, f'{name}.jpg'))
        plot_results(image, name, [], labels_xyxy, to_show=True)

        pass
