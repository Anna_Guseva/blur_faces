import os
import re
import pandas as pd
import numpy as np

if __name__ == "__main__":
    images_path = "../data/images_24_final"
    labels_path = "../../data/labels/with_faces"
    labels_filenames = os.listdir(labels_path)
    save_path = "../../data/sequences"

    labels_numbers = []
    for label_filename in labels_filenames:
        finds = re.findall(r'\d+', label_filename)  # find number of digits through regular expression
        labels_numbers.append(list(map(int, finds))[0])
    labels_numbers_diff = np.array(labels_numbers) - np.array([0] + labels_numbers[:-1])

    df = pd.DataFrame()
    df["filename"] = labels_filenames
    df["number"] = labels_numbers
    df["diff"] = labels_numbers_diff
    df["marker_start"] = np.array(labels_numbers_diff > 100).astype(int)
    df["marker_end"] = df["marker_start"].values.tolist()[1:] + [1]

    df.to_csv("../../data/labels/with_faces_seqs.csv", index=False)

    df_starts = df[df["marker_start"] == 1]
    df_ends = df[df["marker_end"] == 1]

    pass
