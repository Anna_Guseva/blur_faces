import cv2
import numpy as np
from matplotlib import pyplot as plt


def plot_results(image, image_name, boxes_true, boxes_pred,
                 confs=None, save_path=None, to_show=False):
    boxes_true = np.array(boxes_true).astype(int)
    boxes_pred = np.array(boxes_pred).astype(int)

    for bbox in boxes_true:
        image = cv2.rectangle(image, (bbox[0], bbox[1]), (bbox[2], bbox[3]), (0, 255, 0), 2)

    if confs is not None:
        assert len(confs) == boxes_pred.shape[0], "Different number of predictions and confidences"
    else:
        confs = [None] * boxes_true.shape[0]

    for bbox, conf in zip(boxes_pred, confs):
        image = cv2.rectangle(image, (bbox[0], bbox[1]), (bbox[2], bbox[3]), (255, 0, 0), 2)
        if confs is not None:
            image = cv2.putText(image, str(round(conf, 2)),
                                (bbox[0], bbox[1]),
                                cv2.FONT_HERSHEY_SIMPLEX,
                                1, (255, 0, 0), 3, cv2.LINE_AA)

    if to_show:
        cv2.imshow(image_name, image)
        cv2.waitKey(0)

    if save_path is not None:
        cv2.imwrite(save_path, image)

    return image


def plot_compares(image_name, images, axes_names, save_path=None, to_show=False):
    fig = plt.figure(figsize=(30, 30))
    fig.suptitle(image_name)

    n_cols = 2
    n_rows = len(images) // n_cols + int((len(images) % n_cols) > 0.)

    for ind, image in enumerate(images):
        ax1 = fig.add_subplot(n_rows, n_cols, ind+1)
        ax1.set_title(axes_names[ind])
        ax1.imshow(image)

    plt.axis("off")
    plt.tight_layout()

    if to_show:
        plt.show()
    if save_path is not None:
        plt.savefig(save_path)

    plt.clf()
    plt.close()


def blur_rois(image, coords):
    """
    Blur Regions Of Interest
    :return:
    """
    for coord in coords:
        roi = image[coord[1]:coord[3], coord[0]:coord[2]]
        if (roi.shape[0] * roi.shape[1]) > 0:
            roi_blurred = cv2.GaussianBlur(roi, (51, 51), 0)
            image[coord[1]:coord[3], coord[0]:coord[2]] = roi_blurred

    return image
