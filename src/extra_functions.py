import numpy as np


def read_yolo_file(filepath, drop_classes=False):
    with open(filepath, "r") as file:
        labels = file.read()
    labels = labels.split('\n')[:-1]
    labels = np.array(list(map(lambda x: x.split(' '), labels))).astype(float)

    if drop_classes:
        labels = labels[:, 1:]

    return labels
