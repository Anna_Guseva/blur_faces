import os
import json
import numpy as np
from tqdm import trange

from src.extra_functions import coord2xyxy

if __name__ == "__main__":
    labels_path = "../../data/labels/json"
    labels_filenames = os.listdir(labels_path)
    save_path = "../../data/labels/txts"

    for labels_filename in labels_filenames:
        # read labels
        with open(os.path.join(labels_path, labels_filename)) as json_data:
            labels = json.load(json_data)
        labels_images_ids = np.unique(list(map(lambda x: x["image_id"], labels["annotations"])))
        labels_images_dicts = list(filter(lambda x: x["id"] in labels_images_ids, labels["images"]))

        # prepare bboxes
        bboxes_dict = {}
        for annotation in labels["annotations"]:
            if annotation["image_id"] not in list(bboxes_dict.keys()):
                bboxes_dict[annotation["image_id"]] = np.expand_dims(annotation["bbox"], axis=0)
            else:
                bboxes_dict[annotation["image_id"]] = np.vstack(
                    (bboxes_dict[annotation["image_id"]], annotation["bbox"]))

        for ind in trange(len(labels_images_dicts)):
            image_filename = labels_images_dicts[ind]["file_name"]
            image_name = os.path.splitext(image_filename)[0]
            image_ind = labels_images_dicts[ind]["id"]

            # convert bboxes
            bboxes = bboxes_dict[image_ind].astype(int)
            with open(os.path.join(save_path, f"{image_name}.txt"), "w") as file:
                for bbox in bboxes:
                    bbox_xyxy = coord2xyxy(bbox[0], bbox[1], bbox[2], bbox[3],
                                           labels_images_dicts[ind]["height"],
                                           labels_images_dicts[ind]["width"])
                    file.write(f"{bbox_xyxy[0]},{bbox_xyxy[1]},{bbox_xyxy[2]},{bbox_xyxy[3]}\n")
