import os
from pathlib import Path
import pandas as pd
import numpy as np

if __name__ == "__main__":
    json_filepath = "../../models/yolov4/results/sequences.json"
    save_path = "../../models/yolov4/results_txt/sequences"

    json_labels = pd.read_json(json_filepath)
    for ind, row in json_labels.iterrows():
        filename, objects = row["filename"], row["objects"]
        txt_name = Path(filename).stem + ".txt"
        with open(os.path.join(save_path, txt_name), "w") as file:
            for obj in objects:
                relative_coords = np.array(list(obj["relative_coordinates"].values())).astype(float)
                conf = obj["confidence"]
                file.write(f"{conf} {relative_coords[0]} {relative_coords[1]} {relative_coords[2]} {relative_coords[3]}\n")

    pass
