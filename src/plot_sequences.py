from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import matplotlib.gridspec as gridspec
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.widgets import Slider

import os
import glob
import cv2
from natsort import natsorted


def redraw_fn(img_filepath, axes):
    img_bgr = cv2.imread(img_filepath)
    img = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB)
    if not redraw_fn.initialized:
        redraw_fn.im = axes.imshow(img, animated=True)
        redraw_fn.initialized = True
    else:
        redraw_fn.im.set_array(img)


def videofig(images_filepaths, redraw_func,
             play_fps=25, big_scroll=30, key_func=None,
             grid_specs=None, layout_specs=None, figname=None, *args):
        """Figure with horizontal scrollbar and play capabilities

        This script is mainly inspired by the elegant work of João Filipe Henriques
        https://www.mathworks.com/matlabcentral/fileexchange/29544-figure-to-play-and-analyze-videos-with-custom-plots-on-top?focused=5172704&tab=function

        :param num_frames: an integer, number of frames in a sequence
        :param redraw_func: callable with signature redraw_func(f, axes)
                          used to draw a new frame at position f using axes, which is a instance of Axes class in matplotlib
        :param play_fps: an integer, number of frames per second, used to control the play speed
        :param big_scroll: an integer, big scroll number used when pressed page down or page up keys.
        :param key_func: optional callable which signature key_func(key), used to provide custom key shortcuts.
        :param grid_specs: optional dictionary, used to specify the gridspec of the main drawing pane.
                         For example, grid_specs = {'nrows': 2, 'ncols': 2} will create a gridspec with 2 rows and 2 cols.
        :param layout_specs: optional list, used to specify the layout of the gridspec of the main drawing pane.
                         For example, layout_specs = ['[:, 0]', '[:, 1]'] means:
                            gs = ... Some code to create the main drawing pane gridspec ...
                            ax1 = plt.subplot(gs[:, 0])
                            ax2 = plt.subplot(gs[:, 1])
        :param args: other optional arguments
        :return: None
        """
        # Check arguments
        # check_int_scalar(num_frames, 'num_frames')
        check_callback(redraw_func, 'redraw_func')
        check_int_scalar(play_fps, 'play_fps')
        check_int_scalar(big_scroll, 'big_scroll')

        num_frames = len(images_filepaths)

        if key_func:
            check_callback(key_func, 'key_func')

        # Initialize figure
        if not figname:
            figname = 'VideoPlayer'
            fig_handle = plt.figure(num=figname)

        # We use GridSpec to support embedding multiple plots in the main drawing pane.
        # A nested grid demo can be found at https://matplotlib.org/users/plotting/examples/demo_gridspec06.py
        # Construct outer grid, which contains the main drawing pane and slider ball
        outer_grid = gridspec.GridSpec(2, 1,
                                     left=0, bottom=0, right=1, top=1,
                                     height_ratios=[97, 3], wspace=0.0, hspace=0.0)

        # Construct inner grid in main drawing pane
        if grid_specs is None:
            grid_specs = {'nrows': 1, 'ncols': 1}  # We will have only one axes by default

        inner_grid = gridspec.GridSpecFromSubplotSpec(subplot_spec=outer_grid[0], **grid_specs)
        if layout_specs:
            # eval() can't work properly in list comprehension which is inside a function.
            # Refer to: http://bugs.python.org/issue5242
            # Maybe I should find another way to implement layout_specs without using eval()...
            axes_handle = []
            for spec in layout_specs:
                axes_handle.append(plt.Subplot(fig_handle, eval('inner_grid' + spec)))
        else:
            num_inner_plots = grid_specs['nrows'] * grid_specs['ncols']
            axes_handle = [plt.Subplot(fig_handle, inner_grid[i]) for i in range(num_inner_plots)]

        for ax in axes_handle:
            fig_handle.add_subplot(ax)

        if len(axes_handle) == 1:
            axes_handle = axes_handle[0]
            axes_handle.set_axis_off()

        # Build scrollbar
        scroll_axes_handle = plt.Subplot(fig_handle, outer_grid[1])
        scroll_axes_handle.set_facecolor('lightgoldenrodyellow')
        fig_handle.add_subplot(scroll_axes_handle)
        scroll_handle = Slider(scroll_axes_handle, '', 0.0, num_frames - 1, valinit=0.0)

        def draw_new(_):
            redraw_func(images_filepaths[int(scroll_handle.val)], axes_handle)
            fig_handle.canvas.draw_idle()

        def scroll(new_f):
            new_f = min(max(new_f, 0), num_frames - 1)  # clip in the range of [0, num_frames - 1]
            cur_f = scroll_handle.val

            # Stop player at the end of the sequence
            if new_f == (num_frames - 1):
              play.running = False

            if cur_f != new_f:
              # move scroll bar to new position
              scroll_handle.set_val(new_f)

            return axes_handle

        def play(period):
            play.running ^= True  # Toggle state
            if play.running:
              frame_idxs = range(int(scroll_handle.val), num_frames)
              play.anim = FuncAnimation(fig_handle, scroll, frame_idxs,
                                        interval=1000 * period, repeat=False)
              plt.draw()
            else:
              play.anim.event_source.stop()

        # Set initial player state
        play.running = False

        def key_press(event):
            key = event.key
            f = scroll_handle.val
            if key == 'left':
              scroll(f - 1)
            elif key == 'right':
              scroll(f + 1)
            elif key == 'pageup':
              scroll(f - big_scroll)
            elif key == 'pagedown':
              scroll(f + big_scroll)
            elif key == 'home':
              scroll(0)
            elif key == 'end':
              scroll(num_frames - 1)
            elif key == 'enter':
              play(1 / play_fps)
            elif key == 'backspace':
              play(5 / play_fps)
            else:
              if key_func:
                key_func(key)

        # Register events
        scroll_handle.on_changed(draw_new)
        fig_handle.canvas.mpl_connect('key_press_event', key_press)

        # Draw initial frame
        redraw_func(images_filepaths[0], axes_handle)

        # Start playing
        play(1 / play_fps)

        # plt.show() has to be put in the end of the function,
        # otherwise, the program simply won't work, weird...
        plt.show()


def check_int_scalar(a, name):
  assert isinstance(a, int), '{} must be a int scalar, instead of {}'.format(name, type(name))


def check_callback(a, name):
  # Check http://stackoverflow.com/questions/624926/how-to-detect-whether-a-python-variable-is-a-function
  # for more details about python function type detection.
  assert callable(a), '{} must be callable, instead of {}'.format(name, type(name))


if __name__ == "__main__":
    # imgs_folder = "../data/sequences/input"
    # folder_filenames = os.listdir(imgs_folder)
    # imgs_filenames = natsorted(list(filter(lambda x: os.path.splitext(x)[1]==".jpg", folder_filenames)))
    # imgs_filepaths = list(map(lambda x: os.path.join(imgs_folder, x), imgs_filenames))

    # imgs_list_file = "../models/yolov4/data/local/test.txt"
    imgs_list_file = "../models/yolov4/data/with_faces.txt"
    imgs_filepaths = []
    with open(imgs_list_file, "r") as file:
        imgs_filepaths = file.read()
        imgs_filepaths = imgs_filepaths.split('\n')[:-1]

    redraw_fn.initialized = False
    videofig(imgs_filepaths, redraw_fn, play_fps=2)
