import numpy as np


def xyxy2yolo_coords(size, box):
    dw = 1./size[0]
    dh = 1./size[1]

    x = (box[0] + box[2])/2.0
    y = (box[1] + box[3])/2.0
    w = box[2] - box[0]
    h = box[3] - box[1]

    x = x*dw
    y = y*dh
    w = w*dw
    h = h*dh

    return (x,y,w,h)


def yolo_coords2xyxy(coords, image_height, image_width):
    coords_xyxy = []
    for coord in coords:
        [x, y, w, h] = coord

        x_left = max(int((x - w / 2) * image_width), 0)
        y_top = max(int((y - h / 2) * image_height), 0)
        x_right = min(int((x + w / 2) * image_width), image_width - 1)
        y_bottom = min(int((y + h / 2) * image_height), image_height - 1)

        coords_xyxy.append([x_left, y_top, x_right, y_bottom])

    return np.array(coords_xyxy)


def coord2xyxy(x, y, w, h, image_height, image_width):
    x_right = min(int(x + w), image_width - 1)
    y_bottom = min(int(y + h), image_height - 1)

    return int(x), int(y), x_right, y_bottom


def non_max_suppression(boxes, confs, overlapThresh=0.6):
    assert boxes.shape[0] == len(confs), "Different number of boxes and confidences given to non-max suppression function"

    # if there are no boxes, return an empty list
    if len(boxes) == 0:
        return np.array([]), np.array([])
    # if the bounding boxes integers, convert them to floats --
    # this is important since we'll be doing a bunch of divisions
    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")
    # initialize the list of picked indexes
    pick = []
    # grab the coordinates of the bounding boxes
    x1 = boxes[:,0]
    y1 = boxes[:,1]
    x2 = boxes[:,2]
    y2 = boxes[:,3]
    # compute the area of the bounding boxes and sort the bounding
    # boxes by the bottom-right y-coordinate of the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(y2)
    # keep looping while some indexes still remain in the indexes
    # list
    while len(idxs) > 0:
        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)
        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])
        # compute the width and height of the bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)
        # compute the ratio of overlap
        overlap = (w * h) / area[idxs[:last]]
        # delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last],
            np.where(overlap > overlapThresh)[0])))
    # return only the bounding boxes that were picked using the
    # integer data type
    return boxes[pick].astype("int"), confs[pick]


def check_bboxes_area(coords, confs, area_threshold):
    assert coords.shape[0] == len(confs), \
        "Different number of boxes and confidences given to check_bboxes_area function"

    if len(coords) > 0:
        mask = ((coords[:, 2] - coords[:, 0]) * (coords[:, 3] - coords[:, 1])) >= area_threshold
        coords, confs = coords[mask, :], confs[mask]

    return coords, confs
