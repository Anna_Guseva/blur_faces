import os

TEMP_FOLDER = "output/temp"
CURRENT_PATH = os.getcwd()

YOLO_FOLDER = os.path.join(CURRENT_PATH, "models/yolov4")
YOLO_CFG = {
    "darknet_path": r"D:\Pycharm_Projects\test_projects\darknet\darknet.exe",
    "obj_data": os.path.join(YOLO_FOLDER, "cfg/obj.data"),
    "configs": os.path.join(YOLO_FOLDER, "cfg/yolov4.cfg"),
    "weights": os.path.join(YOLO_FOLDER, "trained_models/yolov4_1500.weights"),

    "names": os.path.join(YOLO_FOLDER, "cfg/person.names")
}
YOLO_COCO_CFG = {
    "darknet_path": r"D:\Pycharm_Projects\test_projects\darknet\darknet.exe",
    "obj_data": os.path.join(YOLO_FOLDER, "cfg/coco/obj.data"),
    "configs": os.path.join(YOLO_FOLDER, "cfg/coco/yolov4.cfg"),
    "weights": os.path.join(YOLO_FOLDER, "trained_models/old/yolov4.weights"),

    "names": os.path.join(YOLO_FOLDER, "cfg/coco/coco.names")
}

# update obj_data
with open(YOLO_CFG['obj_data'], "r") as file:
    obj_data = file.read()
    obj_data = obj_data.split('\n')
    obj_data[3] = "names = " + YOLO_CFG["names"]
    obj_data = "\n".join(obj_data)
with open(YOLO_CFG["obj_data"], "w") as file:
    file.writelines(obj_data)
